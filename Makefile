default: image

docker-image = registry.gitlab.com/dhswt/docker/php-nginx

image-dev:
	docker build --squash \
		--build-arg PHP_VERSION=7.4 \
		-t $(docker-image):7.4 \
		-f Dockerfile \
		.

run:
	docker rm -f php-nginx-dev
	docker run -ti \
		--name php-nginx-dev \
		$(docker-image):7.4

enter:
	docker rm -f php-nginx-dev
	docker run -ti \
		--name php-nginx-dev \
		--entrypoint bash \
		$(docker-image):7.4

show-images:
	docker images | grep "$(docker-image)"

clear:
	rm -rf ./build

# Remove dangling images
clean-images:
	docker images -a -q \
		--filter "reference=$(docker-image)" \
		--filter "dangling=true" \
	| xargs docker rmi

# Remove all images
clear-images:
	docker images -a -q \
		--filter "reference=$(docker-image)" \
	| xargs docker rmi
