FROM    registry.gitlab.com/dhswt/docker/s6-base:debian-buster
LABEL   maintainer="David Hiendl <david.hiendl@dhswt.de>"

# image config
EXPOSE  22 80 8888
WORKDIR /www

# variables
ARG     PHP_VERSION
ENV     WEB_HOME="/www" \
        WEB_ROOT="/www/htdocs" \
        ENABLE_CRON=0 \
        ENABLE_SSHD=0 \
        PHP_VERSION="${PHP_VERSION}"

# install basic required utils
RUN     DEBIAN_FRONTEND=noninteractive \
        apt-get update \
&&      apt-get -y install --no-install-recommends \
            lsb-release \
            apt-transport-https \
            ca-certificates \
            curl \
            wget \
            gnupg2 \
            gettext-base \
#
# add nginx repo
&&      echo "deb http://nginx.org/packages/mainline/debian `lsb_release -cs` nginx" | tee /etc/apt/sources.list.d/nginx.list \
&&      curl -fsSL https://nginx.org/keys/nginx_signing.key | apt-key add - \
#
# add php repo
&&      curl -fsSL https://packages.sury.org/php/apt.gpg | apt-key add - \
&&      echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/php.list \
#
# install nginx + php
&&      apt-get update \
&&      apt-get upgrade -y \
&&      apt-get install -y --no-install-recommends \
            nginx \
            php${PHP_VERSION}-fpm \
            php${PHP_VERSION}-opcache \
#
# cleanup apt mess
&&      apt-get clean \
&&      rm -rf \
            /var/lib/apt/lists/* \
            /tmp/* \
            /var/tmp/*


# create clean web root dir
RUN     mkdir -p /www/htdocs \
&&      chown www-data:www-data -R /www \
\
# remove default nginx site and confs
&&      rm -f /etc/nginx/sites-enabled/* \
&&      rm -f /etc/nginx/conf.d/* \
\
# symlink php version to current
&&      ln -s /etc/php/${PHP_VERSION} /etc/php/cur \
\
# configure php-fpm
&&      cd /etc/php/${PHP_VERSION}/fpm \
&&      mv php-fpm.conf php-fpm.conf.bak \
\
# disable original config
&&      rm pool.d/www.conf

# install services, scripts, config
ADD     services.d  /etc/services.d
ADD     php-fpm     /etc/php/${PHP_VERSION}/fpm/
ADD     nginx       /etc/nginx/

# fill in php version
RUN     envsubst '${PHP_VERSION}' < /etc/php/${PHP_VERSION}/fpm/php-fpm.conf.tpl > /etc/php/${PHP_VERSION}/fpm/php-fpm.conf \
&&      rm /etc/php/${PHP_VERSION}/fpm/php-fpm.conf.tpl \
&&      envsubst '${PHP_VERSION}' < /etc/services.d/php-fpm/run.tpl > /etc/services.d/php-fpm/run \
&&      rm /etc/services.d/php-fpm/run.tpl